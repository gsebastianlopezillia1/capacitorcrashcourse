import { Component } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { DomSanitizer } from '@angular/platform-browser';
import { isPlatform } from '@ionic/angular';

const { Browser, Geolocation, Camera, CameraResultType, Contacts } = Plugins;
//import { Contacts } from '@capacitor-community/contacts';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  image = null;
  contacts = [];

  constructor(private sanitazer: DomSanitizer) {
    this.getCurrentPosition();
    this.loadContacts();
  }

  openSettings(){
    return true;
  }
  async loadContacts() {
    if (isPlatform('android')) {
      let permission = await Contacts.getPermission();
      if (!permission.granted) {
        return;
      }
    }
    Contacts.getContacts().then(result => {
      console.log(result);
      this.contacts = result.contacts;
    })
  }

  async getCurrentPosition() {
    const coordinates = await Geolocation.getCurrentPosition();
    console.log('current', coordinates);
    const wait = Geolocation.watchPosition({}, (position, err) => {
      console.log('Changed: ', position)
    })
  }
  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Base64
    });
    this.image = this.sanitazer.bypassSecurityTrustResourceUrl(`data:image/jpeg;base64,${image.base64String}`)
    console.log(image);
  }

  openBrowser() {
    Browser.open({ url: 'https://ionicacademy.com' });
  }
}
