import { Component } from '@angular/core';
import { StatusBarStyle, Plugins } from '@capacitor/core';

import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
  ) {
    this.initializeApp();
  }

  async initializeApp() {
    const { SplashScreen, StatusBar } = Plugins;
    try {
      await SplashScreen.hide();
      await StatusBar.setStyle({ style: StatusBarStyle.Light })
      if (this.platform.is('android')) {
        StatusBar.setBackgroundColor({ color: '#cdcdcd' })
      }
    } catch (err) {
      console.log('This is normal in a browser', err);
    }

  }
}
